package com.shelly.constant;

/**
 * @author: Shelly
 * @create: 2023-03-25 13:07:04
 * @version: 1.0
 * @describe:
 */
public class Constants {
    public static final int COUNT = 1000000;
    public static final int THREAD_COUNT = 100;
}
