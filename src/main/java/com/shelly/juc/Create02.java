package com.shelly.juc;

/**
 * @author: Shelly
 * @create: 2024-01-26 16:49:06
 * @version: 1.0
 * @describe: 实现多线程方法:2、实现Runnable接口
 * 在第一种实现方式中，我们是将线程的创建和线程执行的业务都封装在了Thread对象中，我们可以通过Runable接口来实现线程程序代码和数据有效的分离
 * 实现的步骤：
 * 1. 创建Runable的实现类
 * 2. 重写run方法
 * 3. 创建Runable实例对象(通过实现类来实现)
 * 4. 创建Thread对象，并把第三部的Runable实现作为Thread构造方法的参数
 * 5. 启动线程
 *
 * 实现Runable接口的好处：
 * 1. 可以避免Java单继承带来的局限性
 * 2. 适合多个相同的程序代码处理同一个资源的情况，把线程同程序的代码和数据有效的分离，较好的体现了面向对象的设计思想
 */
public class Create02 {
    /**
     * 线程的第二种方式
     *     本质是创建Thread对象的时候传递了一个Runable接口实现
     * @param args
     */
    public static void main(String[] args) {
        System.out.println("main执行了...");
        // 创建一个新的线程  Thread对象
        Runnable r1 = new RunableTest();
        Thread t1 = new Thread(r1);
        // 启动线程
        t1.start();
        System.out.println("main结束了...");
    }
}
/**
 * 线程的第二种创建方式
 *   创建一个Runable接口的实现类
 */
class RunableTest implements Runnable{
    @Override
    public void run() {
        System.out.println("子线程执行了...");
    }
}