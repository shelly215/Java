package com.shelly.juc;

/**
 * @author: Shelly
 * @create: 2024-01-26 21:56:29
 * @version: 1.0
 * @describe: 线程中的常用方法：1、isAlive  获取线程的状态
 */
public class ThreadMethod01 {
    /**
     * isAlive方法
     * @param args
     */
    public static void main(String[] args) {
        System.out.println("main  start ...");
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName() + " .... ");//Thread-0 ....
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        System.out.println("线程的状态："+t1.isAlive());//false
        t1.start();
        System.out.println("线程的状态："+t1.isAlive());//true
        System.out.println("main  end ...");
    }
}
/*
输出结果：
        main  start ...
        线程的状态：false
        线程的状态：true
        main  end ...
        Thread-0 ....
*/
