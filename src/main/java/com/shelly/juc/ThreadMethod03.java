package com.shelly.juc;

/**
 * @author: Shelly
 * @create: 2024-01-26 21:59:39
 * @version: 1.0
 * @describe: 线程中的常用方法：3、yield
 * 让出CPU，当前线程进入就绪状态
 */
public class ThreadMethod03 extends Thread{
    public ThreadMethod03(String threadName){
        super(threadName);
    }
    /**
     * yield方法  礼让
     * @param args
     */
    public static void main(String[] args) {
        ThreadMethod03 f1 = new ThreadMethod03("A1");
        ThreadMethod03 f2 = new ThreadMethod03("A2");
        ThreadMethod03 f3 = new ThreadMethod03("A3");
        f1.start();
        f2.start();
        f3.start();
    }

    @Override
    public void run() {
        for(int i = 0 ; i < 100; i ++){
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if(i%10 == 0 && i != 0){
                System.out.println(Thread.currentThread().getName()+" 礼让：" + i);
                Thread.currentThread().yield(); // 让出CPU
            }else{
                System.out.println(this.getName() + ":" + i);
            }
        }
    }
}
