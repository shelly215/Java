package com.shelly.juc;

/**
 * @author: Shelly
 * @create: 2024-01-26 21:53:32
 * @version: 1.0
 * @describe: 停止一个正在运行的线程:3、利用InterruptedException
 * 如果线程因为执行join(),sleep()或者wait()而进入阻塞状态，
 * 此时要想停止它，可以让他调用interrupt(),程序会抛出InterruptedException异常。
 * 我们利用这个异常可以来终止线程。
 */
public class StopThread03 extends Thread{
    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new StopThread03();
        t1.start();
        Thread.sleep(3000);
        t1.interrupt(); // 中断线程 将中断标志由false修改为了true
        // t1.stop(); // 直接就把线程给kill掉了
        System.out.println("main .... ");
    }

    @Override
    public void run() {
        System.out.println(this.getName() + " start...");
        int i = 0 ;
        // Thread.interrupted() 如果没有被中断 那么是false 如果显示的执行了interrupt 方法就会修改为 true
        while(!Thread.interrupted()){
            //while(!Thread.currentThread().isInterrupted()){
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
                break;
            }
            System.out.println(this.getName() + " " + i);
            i++;
        }

        System.out.println(this.getName()+ " end .... ");

    }
}
