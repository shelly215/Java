package com.shelly.juc;

/**
 * @author: Shelly
 * @create: 2024-01-26 16:46:25
 * @version: 1.0
 * @describe: 实现多线程方法:1、继承Thread类
 * 实现的步骤：
 * 1. 创建Thread类的子类
 * 2. 重写run方法
 * 3. 创建线程对象
 * 4. 启动线程
 */
public class Create01 {
    /**
     * 线程的第一种实现方式：通过创建Thread类的子类来实现
     * @param args
     */
    public static void main(String[] args) {
        System.out.println("main方法执行了1...");
        // Java中的线程 本质上就是一个Thread对象
        Thread t1 = new ThreadTest01();
        // 启动一个新的线程
        t1.start();
        for(int i = 0 ; i< 100 ; i++){
            System.out.println("main方法的循环..."+i);
        }
        System.out.println("main方法执行结束了3...");
    }
}

/**
 * 线程不能够启动多次，如果要创建多个线程，就创建多个Thread对象即可
 */
class Create0102 {
    public static void main(String[] args) {
        System.out.println("main方法执行了1...");
        Thread t1 = new ThreadTest01();
        //start方法并不能直接开启一个新的线程，真正开启线程的是 CPU，
        //当CPU空闲或者分配到此任务的时候就会创建一个新的线程，然后执行run方法中的代码
        t1.start();
        //t1.start(); 线程不能够启动多次，报错：ILLegalThreadStateException
        Thread t2 = new ThreadTest01();
        t2.start();
        // t1.run(); // 这个是显示的调用Thread的run方法 并没有开启新的线程
        for(int i = 0 ; i< 10 ; i++){
            System.out.println("main方法的循环..." + i);
        }
        System.out.println("main方法执行结束了3...");
    }
}

/**
 * 第一个自定义的线程类
 *    继承Thread父类
 *    重写run方法
 */
class ThreadTest01 extends Thread{
    @Override
    public void run() {
        System.out.println("我们的第一个线程执行了2....");
        for(int i = 0 ; i < 10 ; i ++){
            System.out.println("子线程："+i);
        }
    }
}