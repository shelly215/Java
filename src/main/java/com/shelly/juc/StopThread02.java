package com.shelly.juc;

/**
 * @author: Shelly
 * @create: 2024-01-26 21:50:53
 * @version: 1.0
 * @describe: 停止一个正在运行的线程:2、利用中断标志位
 * 在线程中有个中断的标志位，默认是false，
 * 当我们显示的调用 interrupted方法或者isInterrupted方法是会修改标志位为true。
 * 我们可以利用此来中断运行的线程。
 */
public class StopThread02 extends Thread {
    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new StopThread02();
        t1.start();
        Thread.sleep(3000);
        t1.interrupt(); // 中断线程 将中断标志由false修改为了true
        // t1.stop(); // 直接就把线程给kill掉了
        System.out.println("main .... ");
    }

    @Override
    public void run() {
        System.out.println(this.getName() + " start...");
        int i = 0 ;
        // Thread.interrupted() 如果没有被中断 那么是false 如果显示的执行了interrupt 方法就会修改为 true
        while(!Thread.interrupted()){
            System.out.println(this.getName() + " " + i);
            i++;
        }
        System.out.println(this.getName()+ " end .... ");
    }
}
