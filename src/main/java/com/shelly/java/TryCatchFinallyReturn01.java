package com.shelly.java;

/**
 * @author: Shelly
 * @create: 2024-01-26 21:19:15
 * @version: 1.0
 * @describe:
 * return语句的本质：
 * 1. return语句获取到变量的地址
 * 2. return将获取的地址返回，也就是return本质是传地址
 */
public class TryCatchFinallyReturn01 {
    public static void main(String[] args) {
        Too too=new Too();
        StringBuilder t1=test(too);
        System.out.println("return语句返回的:"+t1+"\t返回值的hashCode:"+t1.hashCode());
        System.out.println("finaly里面修改的:"+too.num+"\tfinaly的hashCode:"+too.num.hashCode());

    }
    public static StringBuilder test(Too too) {
        try {
            too.num=new StringBuilder("try");
            System.out.println("try字符串的hashcode:"+("try").hashCode());
            System.out.println("StringBuilder里的try的hashCode:"+too.num.hashCode());//--语句1
            return too.num; //语句2
        } finally {
            too.num=new StringBuilder("finaly");//语句3
            System.out.println("finaly的hashCode:"+too.num.hashCode());//语句4
        }
    }
}
class Too{
    StringBuilder num=new StringBuilder("你好");
}
/*
输出结果：
try字符串的hashcode:115131
StringBuilder里的try的hashCode:460141958
finaly的hashCode:1163157884
return语句返回的:try	返回值的hashCode:460141958
finaly里面修改的:finaly	finaly的hashCode:1163157884
*/
